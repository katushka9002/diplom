import logo from './logo.svg';
import './App.css';
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Route, NavLink} from 'react-router-dom';
import Main from './components/Main/Main'
import News from './components/News/News'
import Footer from './components/Footer/Footer'
import Header from './components/Header/Header'
import Contacts from './components/Contacts/Contacts';
import NewsDetails from './components/NewsDetails/NewsDetails'
import NewsItem from './components/NewsItem/NewsItem'

const useFetch = () => {
  const [data, updateData] = useState(null);
  const requestUrl='http://newsapi.org/v2/top-headlines?' +
  'country=us&' +
  'apiKey=7dbdcb713f824d1e9a16111108720264';
  useEffect(()=>{
      const fetchData = async () => {
          const response = await axios.get(requestUrl);
          updateData(response.data)
      }
      fetchData();
  }, [])
  return data;
}

function App() {
  const result = useFetch();
  const [news, changeNews] = useState(null);
  
  return (
    <div className="App">
      <Header/>
      <Route
        path='/'
        exact
        render={()=>(
          <Main 
          result = {result}
        />
        )}
      />
      <Route
        path='/News'
        exact
        render={()=>(
          <News 
          result = {result}
        />
        )}
      />
      <Route
        path='/Contacts'
        exact
        component={Contacts}
      />
      <Route
        path='/News/:title'
        exact
        component={NewsDetails}
      />
      <Footer/>
    </div>
  );
}

export default App;
