import React, {useEffect, useState} from 'react';
import {Route, NavLink} from 'react-router-dom';

import '../News/News.css';
import NewsItem from '../NewsItem/NewsItem';


const Main = props => {
    const latestNews = props.result && props.result.articles.filter((item, index)=> {
        return index < 6;
    })
    
    return (
        <div className='news'>
            <div className='container'>
                <div className='news__box'>
                    <h1>Всегда <br/>свежие <span>новости</span></h1>
                    <div className='news__flex'>
                        {
                            props.result && latestNews.map((item,index)=> {
                                return (
                                    <NewsItem
                                        key={index}
                                        title ={item.title}
                                        source = {item.source.name}
                                        dataDay = {new Date(item.publishedAt).getDate()}
                                        dataMonth = {new Date(item.publishedAt).getMonth() + 1}
                                        result = {item}
                                    />

                                )
                            }) 
                        }
                    </div>
                    <div className='news__go'><NavLink to="/News">Быть в курсе событий</NavLink></div>
                </div>
            </div>
        </div>
    )
}

export default Main;