import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <footer className='footer'>
            <div className='container'>
                <div className='footer__box'>
                    <a className='footer__logo' href="/">
                        Новостник
                        <span>Single Page Application</span>
                    </a>
                    <p className='footer__diploma'>
                        Дипломный проект
                    </p>
                    <div className='footer__made'>
                        <span>Made by</span>
                        Екатерина Бурлаченко
                    </div>
                </div>
                
            </div>
            
        </footer>
    )
}

export default Footer;