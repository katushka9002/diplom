import React from 'react'
import './Contacts.css';



const Contacts = () => {
    return (
        <div className='contacts'>
            <div className='container'>
                <div className='contacts__box'>
                    <div className='contacts__info'>
                        <a className='contacts__tel' href='tel:+380634461629'>+38 (063) 446 16 29</a>
                        <div className="contacts__name">
                            Екатерина <br/>Бурлаченко
                        </div>
                        <a className='contacts__mail'>katushka9002@gmail.com</a>
                        <p className='contacts__job'>
                            JavaScript разработчик
                        </p>
                        <p className='contacts__tech'>
                            ES5, ES6, <span>React</span>
                        </p>
                    </div>
                    <div className='contacts__img'>
                        <img src='./img/photo.jpg'/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Contacts;