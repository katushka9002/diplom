import React, {useEffect, useState} from 'react';
import axios from 'axios';
import NewsItem from '../NewsItem/NewsItem';
import NewsDetails from '../NewsDetails/NewsDetails'
import './News.css';



const News = props => {

    return (
        <div className='news'>
            <div className='container'>
                <div className='news__box'>
                    <h1>Быть <br/>в курсе <span>событий</span></h1>
                    <div className='news__flex'>
                        {
                            props.result && props.result.articles.map((item,index)=> {
                                return (
                                    <NewsItem
                                        key={index}
                                        title ={item.title}
                                        source = {item.source.name}
                                        dataDay = {item.publishedAt.substring(8,10)}
                                        dataMonth = {item.publishedAt.substring(5,7)}
                                        result = {item}
                                        
                                    />
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default News;