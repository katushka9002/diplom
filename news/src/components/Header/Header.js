import React from 'react';
import {Route, NavLink} from 'react-router-dom';
import Contacts from '../Contacts/Contacts';
import './Header.css';


const Header = () => {
    return (
        <header className='header'>
            <div className='container'>
                <div className='header__box'>
                    <div className='header__logo'>
                         <NavLink to="/" exact>Новостник</NavLink>
                    </div>
                    <nav>
                        <ul className='header__list'>
                            <li>
                                <NavLink to="/" exact>Главная</NavLink>
                            </li>
                            <li>
                                <NavLink to="/News">Новости</NavLink>
                            </li>
                            <li>
                                <NavLink to="/Contacts">Контакты</NavLink>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            
        </header>
    )
}

export default Header;