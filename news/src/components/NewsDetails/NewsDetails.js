import React from 'react';
import '../News/News.css';
import NewsItem from '../NewsItem/NewsItem';


const NewsDetails = props => {
    function lastWord(words) {
        let n = words.lastIndexOf(" ");
        let lastWord = (words.substring(n+1));
        return  lastWord;
    }
    function allWord(words) {
        let n = words.lastIndexOf(" ");
        let allWords = (words.substring(0, n));
        return allWords
    }
    console.log(props.location.state.publishedAt)
    return (
        <div className='article'>
            <div className='container'>
                <div className='article__box'>
                    <div className='article__info'>

                        <h1>{allWord(props.match.params.title)}<span>{lastWord(props.match.params.title)}</span></h1>
                        <div className='news__source'>
                            {props.location.state.source.name}
                        </div>
                        <div className="news__data">
                            <span className="news__day">
                                {new Date(props.location.state.publishedAt).getDate()}
                            </span>
                            /
                            <span className="news__month">
                                {new Date(props.location.state.publishedAt).getMonth() + 1}
                            </span>
                        </div>
                    </div>
                    <div className="article__content">
                        <img src={props.location.state.urlToImage} className='article__img'/>
                        <p className='article__text'>{props.location.state.description}<br/>{props.location.state.content}</p>
                    </div>
                    
                </div>
            </div>
            
        </div>
    )
}

export default NewsDetails;