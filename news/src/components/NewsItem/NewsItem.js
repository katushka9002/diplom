import React from 'react';
import {withRouter} from 'react-router-dom'
import '../News/News.css';


const NewsItem = props => {
    
    return (
        <div onClick={()=>{
            props.history.push('/News/' + props.title, props.result )
            
        }} className='news__item'>
            <div className="news__title">
                {props.title}
            </div>
            <div className="news__info">
                <div className='news__source'>
                    {props.source}
                </div>
                <div className="news__data">
                    <span className="news__day">
                        {props.dataDay}
                    </span>
                    /
                    <span className="news__month">
                        {props.dataMonth}
                    </span>
                </div>
            </div>
              
        </div>
    )
}

export default withRouter(NewsItem);